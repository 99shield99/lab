#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <string>

using namespace std;

string ferma(long long);

int main() {
    ifstream int_text("input.txt");                                                     // ������� ���������� � ��������� ����� �� ������
    ofstream out_text("output.txt");
   
    if (!int_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }

    unsigned long long array[]{ 0 };                                                    //������ ��� ����� �� ������
    unsigned long long index = 0;                                                       //������ ����� � ������
    char tmp;

    bool error = false;

    int_text.read(&tmp, 1);

    while (!int_text.eof()) {
        if ((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') error = true;        //�������� �� ����������� ������ ��������
        
        if (tmp == ' ' && !error) {
            index++;
            if (index > 0) error = true;                                                //����� ������
        }
        else if (tmp == '\n') {                                                         //����� ������� ��������

            if (!error && index == 0) {                                                 // �������� �� ������������ � �������� �����(���� ��, �� ����������� �� 2)
                for (; array[0] % 2 == 0; array[0] /= 2);
                string result = ferma(array[0]);

                if (result.length() != 0) {
                    out_text << result << endl;
                }
            }

            error = false;
            for (unsigned long long& i : array) i = 0;
            index = 0;
        }
        else {
            if (index <= 1 && !error) {
                array[index] = array[index] * 10 + (tmp-'0');
            }
        }
        int_text.read(&tmp, 1);
    }
    out_text.close();
    int_text.close();
    return 1;
}


bool q(int m, unsigned long long x) {
    vector<int> a;
    for (int i = 1; i < m; i++) a.push_back((i * i) % m);
    for (int r : a) if (x == r) return true;
    return false;
}

int mod(long long n, long long m) {
    long long k = 100 * m;
    for (; n < 0; n += k);
    return n % m;
}

string ferma(long long a) {

    long long x = floor(sqrt(a));
    if (x * x == a) {
        if (x == 1) return "";
        return to_string(x) + " " + to_string(x);
    }

    int ar[]{ 3,4,5 };
    int mass[3][5];

    for (int i = 0; i < 3; i++) 
        for (int j = 0; j < ar[i]; j++)
            if (q(ar[i], mod((j*j - a), ar[i])) || mod((j*j - a), ar[i]) == 0) mass[i][j] = 1;
            else mass[i][j] = 0;


    x++;
    int r[3];
    for (int i = 0; i < 3; i++) r[i] = x % ar[i];


four:
    bool flag = true;
    for (int i = 0; i < 3; i++)
        if (mass[i][r[i]] == 0) flag = false;


    if (flag) {
        long long z = x * x - a;
        long long y = floor(sqrt(z));
        if (y * y == z) {
            if (x + y == 1 || x - y == 1) return "";
            return to_string(x + y) + " " + to_string(x - y);
        }
    }

    x++;
    if (x > (a+9)/6) return "";
    for (int i = 0; i < 3; i++) r[i] = (r[i] + 1) % ar[i];


    goto four;
}