#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;


void throwi(unsigned long long int b) {
    int bits = 0;
    while (b != 0) {
        bits += (b & 1u);
        b >>= 1u;
    }
    if (bits != 1) throw std::exception();
}

unsigned long long MP(unsigned long long x, unsigned long long y, unsigned long long m, int power, int k) {
    int b = pow(2, power), mask, newM = 1;
    mask = b - 1;
    while ((newM * (m & mask)) % b != 1) newM++;
    newM = -newM + b;
    unsigned long long z = 0, xi, u;
    for (int i = 0u; i < k; i++) {
        xi = (x >> power * i) & mask;
        u = ((z & mask) + xi * (y & mask)) * newM % b;
        z = (z + xi * y + u * m) / b;
    }
    if (z > m) z -= m;
    return z;
}

unsigned long long MR(unsigned long long x, unsigned long long m, int power, int k) {
    int b = pow(2, power);
    int mask = b - 1;
    int newM = 1;
    while ((newM * (m & mask)) % b != 1) newM++;
    newM = -newM + b;
    unsigned long long z = x;
    for (int i = 0; i < k; i++) {
        unsigned long long zi = (z >> power * i) & mask;
        int u = (zi * newM) % b;
        z = z + u * m * pow(b, i);
    }
    z = z / (int)(pow(b, k));
    if (z >= m) z -= m;
    return z;
}

unsigned long long ME(unsigned long long x, unsigned long long y, unsigned long long m, unsigned long long b) {
    unsigned int power = 1, k = 0, n = 0, R2D2 = 1, newX, z;
    while (pow(2, power) != b) power++;
    while ((m >> power * k) != 0) k++;
    while ((y >> n) != 0) n++;
    for (int i = 0; i < 2 * k; i++) R2D2 = R2D2 * b % m;
    newX = MP(x, R2D2, m, power, k);
    z = newX;
    for (int i = n - 2; i >= 0; i--) {
        z = MP(z, z, m, power, k);
        if (((y >> i) & 1) == 1) z = MP(z, newX, m, power, k);
    }
    return MR(z, m, power, k);
}


int main() {
    ifstream int_text("input.txt");                                 // �������� �������� � �� ����� � ������ ��� ������ � ������    
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }
    char a;
    int_text.read(&a, 1);

    unsigned long long array[]{ 0,0,0,0 }, index = 0;                   //������ ��� ����� �� ������ 
    bool error = false;


        while (!int_text.eof()) {
            if ((a < '0' || a > '9') && a != ' ' && a != '\n') {
                error = true;
            }

            switch (a)
            {
            case '\n':
            {
                if (!error && array[1] >= 1 && array[0] < array[2]) {
                    try {

                        throwi(array[3]);
                        unsigned long long result = ME(array[0], array[1], array[2], array[3]);
                        out_text << result << endl;
                    }
                    catch (std::exception& e) {}
                }
                for (unsigned long long& i : array) i = 0;
                index = 0;
                error = false;
                break;
            }

            case ' ':
            {
                index++;
                if (index >= 4) error = true;
                break;
            }

            default:
            {
                if (index < 4) array[index] = array[index] * 10 + (a - '0');
                break;
            }
            }
            int_text.read(&a, 1);
        }
    out_text.close();
    int_text.close();
    return 1;
}
