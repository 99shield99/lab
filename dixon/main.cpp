#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <numeric>
#include <vector>

using namespace std;

int dixon(long long, long long);

bool isPrime(unsigned long a) {
    unsigned long i;
    if (a == 2)
        return true;
    if (a == 0 || a == 1 || a % 2 == 0)
        return false;
    for (i = 3; i * i <= a && a % i; i += 2)
        ;
    return i * i > a;
}


int GCD(int a, int b) {
    if (b == 0) return a;
    return GCD(b, a % b);
}

int Dixon(long long n, long long k) {
    vector<int> base(k);
    base[0] = 2;
    base[1] = 3;
    for (int i = 2; i < k; i++) {
        base[i] = base[i - 1];
        bool isFine = false;
        while (!isFine) {
            bool isFineTemp = true;
            base[i] += 2;
            for (int j = 1; j < i; j++) {
                if (base[i] % base[j] == 0) {
                    isFineTemp = false;
                    break;
                }
            }
            if (isFineTemp) isFine = true;
        }
    }
    int t = k + 1;

    vector<long long> a;
    vector<long long> b;

    for (int i = 0; i < t; i++) {
        a.push_back(0);
        b.push_back(0);
    }

    vector<vector<int>> l(t, vector<int>(k));

    for (int i = 0; i < t; i++) {
        long long tempB;
        do {
            a[i] = rand() % n;
            b[i] = (a[i] * a[i]) % n;
            for (int j = 0; j < k; j++) l[i][j] = 0;
            tempB = b[i];
            if (b[i] != 0)
                for (int j = 0; j < k; j++)
                    while (tempB % base[j] == 0) {
                        l[i][j]++;
                        tempB = tempB / base[j];
                    }
            else tempB = 1000;

        } while (tempB != 1);
    }

sh:
    vector < vector <int> > V(t, vector <int>(k));
    for (int i = 0; i < t; i++) for (int j = 0; j < k; j++) V[i][j] = l[i][j] % 2;

    vector<int> c(t, 0);
    int temp = 1;
    while (temp < pow(2, t)) {
        for (int i = t - 1; i >= 0; i--) {
            c[i] = 0;
            c[i] = (temp >> i) & 1;
        }
        bool isSolution = true;
        for (int i = 0; i < k; i++) {
            int sum = 0;
            for (int j = 0; j < t; j++) sum += c[j] * V[j][i];
            if (sum % 2 != 0) isSolution = false;
        }
        if (isSolution) {
            int x = 1;
            int y = 1;
            for (int i = 0; i < t; i++) if (c[i] == 1) x *= a[i];
            x = x % n;
            for (int j = 0; j < k; j++) {
                int p = 0;
                for (int i = 0; i < t; i++)  if (c[i] == 1) p += l[i][j];
                p = p / 2;
                y *= pow(base[j], p);
            }
            if ((x%n)!=(y%n) && (x+y) % n != 0) {
                cout << GCD(abs(x - y), n) << ", " << GCD(abs(x + y), n) << endl;
                return GCD(x - y, n);
            }
        }
        temp++;
    }
    t++;
    l.emplace_back(k);
    a.push_back(0);
    b.push_back(0);
   
    long long temp;
    do {
        a[a.size()- 1] = rand() % n;
        b[a.size()-1] = (a[a.size()- 1]*a[a.size()-1])%n;

        for (int j = 0; j < k; j++) l[a.size() - 1][j] = 0;

        temp = b[a.size() - 1];

        if (b[a.size() - 1] != 0)
            for (int j = 0; j < k; j++)
                while (temp % base[j] == 0) {
                    l[a.size() - 1][j]++;
                    temp = temp/base[j];
                }
        else temp = 1000;
    } while (temp != 1);
    goto sh;
}

int main() {
    ifstream int_text("input.txt");                                                     // ������� ���������� � ��������� ����� �� ������
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }

    srand(time(nullptr));                                                               //c����������

    unsigned long long array[]{ 0,0 };                                                  //������ ��� ����� �� ������
    unsigned long long index = 0;                                                       //������ ����� � ������
    char tmp;

    int_text.read(&tmp, 1);
    bool error = false;
    
    while (!int_text.eof()) {
        if ((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') error = true;        //�������� �� ����������� ������ ��������
            

        if (tmp == ' ' && !error) {                                                     // ������� �� ������        
            index++;
            if (index > 1) error = true;
        }
        else if (tmp == '\n') {                                                         //������z ��������
            unsigned long long s = floor(sqrt(array[0]));
            if (!error && index == 1 && array[1] >= 2 && array[1] <= s && !isPrime(array[0])) {         //�������� ������� ��� ���������� ��������� �������
                for (; array[0] % 2 == 0; array[0] /= 2);
                int result = Dixon(array[0], array[1]);
                cout << result << endl;
                out_text << result << endl;
            }
            error = false;
            for (unsigned long long& i : array) i = 0;
            index = 0;
        }
        else {
            if (index <= 1 && error == false) array[index] = array[index] * 10 + (tmp-'0');
        }
        int_text.read(&tmp, 1);
    }
    out_text.close();
    int_text.close();
    return 1;
}