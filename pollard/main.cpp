#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <array>
#include <algorithm>
#include <numeric>
#include <vector>

using namespace std;

bool P(unsigned long a) {
    unsigned long b;
    if (a == 2) return true;
    if (a == 0 || a == 1 || a % 2 == 0) return false;
    for (b = 3; b * b <= a && a % b; b += 2);
    return b * b > a;
}

int GCD(int a, int b) {
    if (b == 0) return a;
    return GCD(b, a % b);
}

int Generator(int h) {
    int p = h - 1;
    
    vector<int> vec;
    
    if (p % 2 == 0) {
        vec.push_back(2);
        while (p % 2 == 0) p /= 2;
    }
    for (int i = 3; i < h && p != 1; i += 2) 
        if (p % i == 0) {
            vec.push_back(i);
            while (p % i == 0) p /= i;
        }
    for (int i = 2; i < h; i++) {
        bool isGenerator = true;
        for (int q : vec) {
            int k = 1;
            for (int j = 1; j <= ((h - 1) / q); j++)  k = k * i % h;
            if (k % h == 1) isGenerator = false;
        }
        if (isGenerator) return i;
    }
    return 0;
}

struct XYZ {
    int x;
    int z;
    int y;

    XYZ(int x1, int y1, int z1) {
        x = x1;
        y = y1;
        z = z1;
    }
};

int nextX(int x, int a, int n, int g) {
    if (x % 3 == 1) return (a * x) % n;
    else if (x % 3 == 2) return (x * x) % n;
    else return (g * x) % n;
}

int nextY(int y, int x, int n) {
    if (x % 3 == 1) return (y + 1) % (n - 1);
    else if (x % 3 == 2) return (2 * y) % (n - 1);
    else return (y) % (n - 1);
}

int nextZ(int z, int x, int n) {
    if (x % 3 == 1) return (z) % (n - 1);
    else if (x % 3 == 2) return (2 * z) % (n - 1);
    else return (z + 1) % (n - 1);
}

XYZ st(XYZ xyz, int a, int g, int n) {
    XYZ res(0, 0, 0);
    res.x = nextX(xyz.x, a, n, g);
    res.y = nextY(xyz.y, xyz.x, n);
    res.z = nextZ(xyz.z, xyz.x, n);
    return res;
}

int Pollard(int n, int a) {
    XYZ s1(1, 0, 0);
    XYZ s2(1, 0, 0);

    int g = Generator(n);
    do {
        s1 = st(s1, a, g, n);
        s2 = st(st(s2, a, g, n), a, g, n);
    } while (s1.x != s2.x);
    if (s1.y == s2.y) {
        cout << "error" << std::endl;
        return -1;
    }
    else {
        int d = GCD((n - 1 + s1.y - s2.y) % (n - 1), n - 1);
        vector<int> b;
        for (int i = 1; i < n; i++) {
            int left = ((n - 1 + s1.y - s2.y) * i) % (n - 1);
            int right = (n - 1 + s2.z - s1.z) % (n - 1);
            if (left == right) {
                b.push_back(i);
            }
        }
        for (int i = 0; i < d; i++) {
            int k = 1;
            for (int j = 1; j <= b[i]; j++) {
                k = k * g % n;
            }
            if (k == a) {
                return b[i];
            }
        }
    }
    return -1;
}

int main() {
    srand(time(nullptr));
    ifstream int_text("input.txt");                                                     // ������� ���������� � ��������� ����� �� ������
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }
    unsigned long long  array[]{ 0,0 };                                                 //������
    unsigned long long readingNumber = 0;                                               //������ 
    
    char tmp;

    int_text.read(&tmp, 1);
    bool error = false;

    while (!int_text.eof()) {
        if ((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') {                    //�������� �� ����������� ������ ��������
            error = true;
        }
        if (tmp == ' ' && !error) {
            readingNumber++;
            if (readingNumber > 1) {                                                    //����� ������
                error = true;
            }
        }
        else if (tmp == '\n') {                                                         //������� � �������� 
            if (!error && readingNumber == 1 && array[1] < array[0] && P(array[0])) {
                int result = Pollard(array[0], array[1]);
                if (result != -1) {
                    out_text << result << endl;
                }
            }
            error = false;
            for (unsigned long long& i : array) i = 0;
            readingNumber = 0;
        }
        else {
            if (readingNumber <= 1 && !error) {
                array[readingNumber] = array[readingNumber] * 10 + (tmp-'0');
            }
        }
        int_text.read(&tmp, 1);
    }
    out_text.close();
    int_text.close();
    return 1;
}
