#include <iostream>
#include <fstream>

using namespace std;

int left_to_right(unsigned int x, unsigned int y, unsigned int m) {
    int xx = x;
    int t = 2;
    int n = 0;

    if (x == 0) return 0;
    if (y == 0) return 1;

    for (; y >= t; n++) {
        t *= 2;
    }

    n++;

    for (int i = n - 2; i >= 0; i--) {
        xx = (xx * xx) % m;

        if (((y >> i) & 1) == 1) {
            xx = (xx * x) % m;
        }
    }
    return xx;
}

unsigned int extracting(unsigned int a, unsigned int n) {
    unsigned int a0 = a;
    unsigned int a1 = a;
    do {
        a0 = a1;
        a1 = ((a / left_to_right(a0, n - 1, -1)) + (n - 1) * a0) / n;
    } while (a0 > a1);
    return a0;
}


int main() {
    ifstream int_text("input.txt");                                 // �������� �������� � �� ����� � ������ ��� ������ � ������    
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }
    char a;
    int_text.read(&a, 1);

    int array[]{ 0,0 }, index = 0;                   //������ ��� ����� �� ������ 
    bool error = false;

    while (!int_text.eof()) {
        if ((a < '0' || a > '9') && a != ' ' && a != '\n') {
            error = true;
        }

        switch (a)
        {
            case '\n':
            {
                if (!error) {
                    unsigned long long result = extracting(array[0], array[1]);
                    out_text << result << endl;
                }
                for (int& i : array) i = 0;
                index = 0;
                error = false;
                break;
            }

            case ' ':
            {
                index++;
                if (index > 1) error = true;
                break;
            }

            default:
            {
                if (index <= 1) array[index] = array[index] * 10 + (a - '0');
                break;
            }
        }
        int_text.read(&a, 1);
    }
    out_text.close();
    int_text.close();
    return 1;
}