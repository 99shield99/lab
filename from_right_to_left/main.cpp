#include <iostream>
#include <fstream>

using namespace std;

unsigned long long right_to_left(unsigned long long x, unsigned long long y, unsigned long long m) {
    unsigned long long xx = x;
    unsigned long long result = x;
    unsigned long t = 2;
    unsigned long n = 0;

   // if (x == 0) return 0;
   // if (y == 0) return 1;

    if ((y & 1) == 0) {
        result = 1;
    }

    for (; y >= t; n++) {
        t *= 2;
    }

    n++;

    for (int i = 1; i < n; i++) {
        xx = (xx * xx) % m;

        if (((y >> i) & 1) == 1) {
            result = (result * xx) % m;
        }
    }
    return result;
}


int main() {

    ifstream int_text("input.txt");                  // �������� �������� � �� ����� � ������ ��� ������ � ������    
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                      // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                      // ���� �� ������� ������� ����
        return 0;
    }

    bool f = false;                                 //����, ��� ������ ����� while
    unsigned long long element[3];                  //������� ������ ��� ��������

    for (int i = 0; i < 3; i++) {                   //�������� ������
        element[i] = 0;
    }

    int count = 0;                                  //��������
    char tmp;                                       //��������� ����������

    int_text.read(&tmp, 1);                         //������ �����                

   const char space = ' ';
   const char enter = '\n';

    while (!int_text.eof()) {                                           //���� ���� ������, ����� ��������
        if ((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n')      //�������� �� ����� � �������
            f = true;

        switch (tmp) {
        case space:
                count++;
                if (count > 2) {                    //������ ��������
                    f = true;
                }
            break;

        case enter:                                  //���� ���� ������, ��
            if (f == false && element[2] != 0) {
                out_text << right_to_left(element[0], element[1], element[2]) << endl;         //������ � ���� ����������
                cout << right_to_left(element[0], element[1], element[2]) << endl;
            }
            f = false;
            count = 0;                                                                          //��������� ��������� ��� ���������� ����
            for (int i = 0; i < 3; i++) {                                                       //��������� �������� �������, ��� ���� ����
                element[i] = 0;
            }
            break;

        default:
            if (count <= 2)
                element[count] = element[count] * 10 + (tmp - '0');
            break;
        }
        int_text.read(&tmp, 1);
    }

    int_text.close();
    out_text.close();

    return 1;
}