#include <iostream>
#include <fstream>

using namespace std;

unsigned long long Barrett_method(unsigned long long x, unsigned long long m) {
    if (x == m) return 0;
    unsigned long long k = 0;
    unsigned long long max = m;
    while (max != 0) {
        max >>= 2u;
        k++;
    }


    unsigned long long n = 0;
    max = x;
    while (max != 0) {
        max >>= 2u;
        n++;
    }
    if (n > 2 * k) throw std::exception();

    unsigned long long z = (1ull << (4 * k)) / m;
    unsigned long long tx = x >> 2 * (k - 1);
    unsigned long long q = (tx * z) >> 2 * (k + 1);
    unsigned long long mask = (1u << (2 * (k + 1))) - 1;
    unsigned long long r1 = (x & mask);
    unsigned long long r2 = ((q * m) & mask);
    unsigned long long r = 0;
    if (r1 >= r2) r = r1 - r2;
    else r = (1u << 2 * (k + 1)) + r1 - r2;
    while (r >= m) {
        r -= m;
    }
    return r;
}

int main() {
    ifstream int_text("input.txt");                                 // �������� �������� � �� ����� � ������ ��� ������ � ������    
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }
    char a;
    int_text.read(&a, 1);

    unsigned long long array[]{ 0,0 }, index = 0;                   //������ ��� ����� �� ������ 
    bool error = false;

    while (!int_text.eof()) {
        if ((a < '0' || a > '9') && a != ' ' && a != '\n') {
            error = true;
        }

        switch (a)
        {
            case '\n':
            {
                if (!error && array[1] >= 1) {
                    try {
                        unsigned long long result = Barrett_method(array[0], array[1]);
                        out_text << result << endl;
                    }
                    catch (std::exception& e) {}
                }
                for (unsigned long long& i : array) i = 0;
                index = 0;
                error = false;
                break;
            }

            case ' ':
            {
                index++;
                if (index > 1) error = true;
                break;
            }

            default:
            {
                if (index <= 1) array[index] = array[index] * 10 + (a - '0');
                break;
            }
        }
        int_text.read(&a, 1);
    }

    out_text.close();
    int_text.close();
    return 1;
}