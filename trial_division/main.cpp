#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>              // ��� �������������� ����� ������ 
using namespace std;

string trial(int n, int b) {
    int r = 0, q = 0, d = 3, num = n;
    string res = "";

    while (d <= b && n != 1) {
        r = n % d;
        q = n / d;

        if (r == 0) {
            res += to_string(d) + " ";
            n = q;
        }
        else if (d < q) {
            d += 2;
        }
        else {
            res += to_string(n) + " ";
            n = 1;
        }
    }
    res += to_string(n);
    
    if (n > 1)
        res += " 1";
    else
        res += " 0";
    
    return res;
}


int main() {
    ifstream int_text("input.txt");                                 // �������� �������� � �� ����� � ������ ��� ������ � ������    
    ofstream out_text("output.txt");
    if (!int_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }
    if (!out_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }
   
    unsigned long long array[2];
    int index = 0;
    char a;
    int_text.read(&a, 1);
    bool error = false;

    for (int i = 0; i < 2; i++)
        array[i] = 0;

    while (!int_text.eof()) {
        if ((a < '0' || a > '9') && a != ' ' && a != '\n')
            error = true;

        if (a == ' ') {
            index++;
            if (index > 1) {
                error = true;
            }

        }
        else if (a == '\n') {
            if (!error && index == 1) {
                int n = array[0];
                string result = "";
                while ((n & 1) == 0) {
                    result += "2 ";
                    n = n >> 1;
                }
                result += trial(n, array[1]);
                out_text << result << endl;
            }

            error = false;
            for (int i = 0; i < 2; i++)
                array[i] = 0;
            index = 0;
        }
        else {
            if (index <= 1)
                array[index] = array[index] * 10 + (a - '0');

        }
        int_text.read(&a, 1);
    }
    out_text.close();
    int_text.close();

    return 1;
}
