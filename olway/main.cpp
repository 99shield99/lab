#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <sstream>

using namespace std;

int from_left_to_right(unsigned long long xx, unsigned long long yy) {
    if (yy == 0) return 1;
    if (xx == 0) return 0;
    
    unsigned long long z = xx;
    long k = 2, n = 0;
    for (; yy >= k; n++) k *= 2;
    
    n++;
    
    for (int i = n - 2; i >= 0; i--) {
        z = (z * z);
        if (((yy >> i) & 1) == 1) z = (z * xx);
    }

    return z;
}

unsigned long long kubkor(unsigned long long a, unsigned long long n) {
    unsigned long long a0 = a;
    unsigned long long a1 = ((a / (from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    while (a0 > a1) {
        a0 = a1;
        a1 = ((a / (from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    }
    return a0;

}

string olw(int n) {

    int cc = kubkor(n, 3);
    int d = 2 * cc + 1;

    int r1 = n % d;
    int r2 = n % (d - 2);
    int q = 4 * (floor(n / (d - 2)) - floor(n / d));
    int s = kubkor(n, 2);

    int r = 2 * r1 - r2 + q;
    while (true) {
        d += 2;
        r = 2 * r1 - r2 + q;
        if (d > s)  return "";
        if (r < 0)
        {
            r += d;
            q += 4;
        }
        while (r >= d)
        {
            r -= d;
            q -= 4;
        }
        if (r == 0) return to_string(d);
        else {
            r2 = r1;
            r1 = r;
        }
    }
}

int main() {
    ifstream int_text("input.txt");                  // �������� �������� � �� ����� � ������ ��� ������ � ������    
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                      // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                      // ���� �� ������� ������� ����
        return 0;
    }

    unsigned long long array[1];
    int index = 0;
    char a;
    int_text.read(&a, 1);
    bool flag = false;
    for (int i = 0; i < 1; i++)
        array[i] = 0;
    while (!int_text.eof()) {
        if ((a < '0' || a > '9') && a != ' ' && a != '\n')
            flag = true;

        if (a == ' ' && flag == false) {
            index++;
            if (index > 0) {
                flag = true;
            }
        }
        else if (a == '\n') {
            if (!flag && index == 0 && array[0] > 0) {
                int n = array[0];
                int even_dividers = 0;
                string result = "";
                while ((n & 1) == 0) {
                    n = n >> 1;
                    even_dividers += 2;
                }
                result = olw(n);
                if (result != "")
                    out_text << result << endl;
            }

            flag = false;
            for (int i = 0; i < 1; i++)
                array[i] = 0;
            index = 0;
        }
        else {
            if (index <= 1 && flag == false)
                array[index] = array[index] * 10 + (a - '0');

        }
        int_text.read(&a, 1);
    }
    out_text.close();
    int_text.close();
    return 1;
}
