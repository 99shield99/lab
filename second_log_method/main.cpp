#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <numeric>
#include <vector>
#include <array>
#include <algorithm>

using namespace std;

bool P(unsigned long a) {
    unsigned long i;
    if (a == 2) return true;
    if (a == 0 || a == 1 || a % 2 == 0)return false;
    for (i = 3; i * i <= a && a % i; i += 2);
    return i * i > a;
}

int phi(int n) {
    int result = n;
    for (int i = 2; i * i <= n; ++i)
        if (n % i == 0) {
            while (n % i == 0)
                n /= i;
            result -= result / i;
        }
    if (n > 1) result -= result/n;
    return result;
}

int pow(int x, int y, int n) {
    while (y < 0) y += phi(n);
    
    int k = 1;
    for (int l = 1; l <= y; l++) k = k * x % n;

    return k;
}

vector<int> VV(int p) {
    vector<int> d;
    if (p % 2 == 0) {
        d.push_back(2);
        while (p % 2 == 0) p /= 2;
    }
    for (int i = 3; i <= p && p != 1; i += 2) {
        if (p % i == 0) {
            d.push_back(i);
            while (p % i == 0) p /= i;
        }
    }
    return d;
}

vector<int> getE(int p, const vector<int>& factors) {
    vector<int> ll;
    for (auto d : factors) {
        ll.push_back(0);
        while (p % d == 0) {
            ll[ll.size() - 1]++;
            p /= d;
        }
    }
    return ll;
}

int Generator(int n) {
    auto d = VV(n - 1);
    for (int i = 2; i < n; i++) {
        bool ggenerator = true;
        for (int q : d) if (pow(i, ((n - 1) / q), n) % n == 1) ggenerator = false;
        if (ggenerator) return i;
    }
    return 0;
}

int findx0(int a, int n, int p, const vector<vector<int>>& k, int i) {
    int b = pow(a, (n - 1) / p, n);
    for (int j = 0; j < p; j++) if (b == k[i][j])return j;
    return -1;
}

int findxj(int a, int g, int y, int p, int i, int j, const vector<vector<int>>& o, int n) {
    int b = pow(a * pow(g, -y, n), n / (pow(p, j + 1)), n);
    for (int k = 0; k < p; k++) if (b == o[i][k]) return k;
    return -1;
}

int Pollig(int n, int a) {
    auto p = VV(n - 1);
    int g = Generator(n);

    vector<vector<int>> RR(p.size(), vector<int>(0));
    vector<int> AA;
    for (int i = 0; i < p.size(); i++) {
        AA.push_back(pow(g, (n - 1) / p[i], n));
        for (int j = 0; j < p[i]; j++) RR[i].push_back(pow(AA[i], j, n));
    }

    vector<int> XX(p.size(), 0);
    auto e = getE(n - 1, p);
    for (int i = 0; i < p.size(); i++) {
        int y = 0;
        int x0 = findx0(a, n, p[i], RR, i);
        y += x0;

        for (int j = 1; j < e[i]; j++) {
            int x = findxj(a, g, y, p[i], i, j, RR, n);
            y += x * pow(p[i], j);
        }
        XX[i] = y;
    }

    for (int i = 0; i < XX.size(); i++);
    int res = 0;
    for (int i = 0; i < p.size(); i++) {
        int m = pow(p[i], e[i]);
        int M = (n - 1) / m;
        int Ms = pow(M, -1, m);
        res += XX[i] * M * Ms % (n - 1);
    }
    res %= n - 1;
    return res;
}

int main() {
    srand(time(nullptr));
    srand(time(nullptr));
    
    ifstream int_text("input.txt");                                                     // ������� ���������� � ��������� ����� �� ������
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                                          // ���� �� ������� ������� ����
        return 0;
    }

    unsigned long long array[]{ 0,0 };                                                  //������
    unsigned long long Number = 0;                                                      //������
    char tmp;
    int_text.read(&tmp, 1);
    bool error = false;

    while (!int_text.eof()) {
        if ((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') error = true;                    //�������� �� ����������� ������ ��������
        if (tmp == ' ' && !error) {
            Number++;
            if (Number > 1) error = true;                                               //�������� �� ����� ������ (� ���-�� �����)
        }
        else if (tmp == '\n') {                                                     
            if (!error && Number == 1 && array[1] < array[0] && P(array[0])) {
                int result = Pollig(array[0], array[1]);
                if (result != -1) out_text << result << endl;
            }
            error = false;
            for (unsigned long long& i : array) i = 0;
            Number = 0;
        }
        else if (Number <= 1 && !error) array[Number] = array[Number] * 10 + (tmp - '0');
        int_text.read(&tmp, 1);
    }
    out_text.close();
    int_text.close();
    return 1;
}
