#include <iostream>
#include <fstream>

using namespace std;

unsigned long long karatsuba(long long u, long long m)
{
    unsigned long long max = u, i = 0;;
    if (u < m) max = m;

    while (max != 0) {
        max >>= 1u;
        i++;
    }

    unsigned long long n, mask, u0, v0, u1, v1, A, B, C, z;

    if (i % 2 == 0) n = i / 2;
    else n = i / 2 + 1;

    u0 = u & ((1u << n) - 1);
    v0 = m & ((1u << n) - 1);
    u1 = u >> n;
    v1 = m >> n;

    return (((u1 * v1) << (2 * n)) + (((u1 + u0) * (v1 + v0) - (u1 * v1) - (u0 * v0)) << n) + (u0 * v0));
}

int main() {
    ifstream int_text("input.txt");                                 // �������� �������� � �� ����� � ������ ��� ������ � ������    
    ofstream out_text("output.txt");

    if (!int_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }

    if (!out_text.is_open()) {                                      // ���� �� ������� ������� ����
        return 0;
    }
    char a;
    int_text.read(&a, 1);

    unsigned long long array[]{ 0,0 }, index = 0;                   //������ ��� ����� �� ������ 
    bool error = false;

    while (!int_text.eof()) {
        if ((a < '0' || a > '9') && a != ' ' && a != '\n') {
            error = true;
        }

        switch (a)
        {
            case '\n':
            {
                if (!error && index == 1) {
                    unsigned long long result = karatsuba(array[0], array[1]);
                    out_text << result << endl;
                }
                for (unsigned long long& i : array) i = 0;
                index = 0;
                error = false;
                break;
            }
        
            case ' ':
            {
                index++;
                if (index > 1) error = true;
                break;
            }

            default:
            {
                if (index <= 1) array[index] = array[index] * 10 + (a - '0');
                break;
            }
        }
        int_text.read(&a, 1);
    }

    out_text.close();
    int_text.close();
    return 1;
 
}